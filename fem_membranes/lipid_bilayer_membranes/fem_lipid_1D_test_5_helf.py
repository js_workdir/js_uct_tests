# -------------------------
# fem_lipid_1D_test_5_helf.py
# -------------------------

# -------------------------
# Description:
# - Based on article Finite element modeling of lipid bilayer membranes
# - Version with lagrange multipliers
# - Version of Helfrich
#
# Last edit: 04.05. 2023
# -------------------------

import fenics
import fenics as fe
import matplotlib.pyplot as plt
import mpi4py
import ufl
import math
import numpy as np


def lipid_transformation_solver(type="lagrange"):
    ##############################
    # Numerical parameters
    ##############################
    num_elements = 1000  # Number of elements
    v_deg = 1  # Degree of driving term
    ex_deg = 100  # Degree of expression
    tol = 1.0e-14  # Numerical tolerance
    newton_iters = 1  # Number of Newton iters
    s_start = -1  # Start point x
    s_end = 1  # End point x

    ##############################
    # Material parameters
    ##############################
    r_init = 2.0e-6  # Radius
    ks = 0.1  # Material parameter
    kb = 0.1  # Material parameter
    d = 1.2e-9  # Half thickness of membrane

    ##############################
    # Pre-calculated values
    ##############################
    d_area_s = 16*math.pi*d*r_init  # Sphere surface ares
    V_s = 4/3*math.pi*r_init**3  # Sphere volume

    ##############################
    # Classes
    ##############################
    # Definition of start point
    def left_point(x):
        return fe.near(x[0], s_start)

    # Definition of end point
    def right_point(x):
        return fe.near(x[0], s_end)

    ##############################
    # Mesh constructor
    ##############################
    mesh = fe.IntervalMesh(num_elements, s_start, s_end)

    ##############################
    # Function spaces
    ##############################
    #V = fe.FunctionSpace(mesh, "P", v_deg)
    P1 = fe.FiniteElement("P", fe.interval, v_deg)
    #R1 = fe.FiniteElement("Real", fe.interval, 0)
    if type == "lagrange":
        element = fe.MixedElement([P1, P1])
    else:
        element = fe.MixedElement([P1, P1])
    Vmix = fe.FunctionSpace(mesh, element)
    Vmix = fe.FunctionSpace(mesh, "P", v_deg)
    sol = fe.Function(Vmix)

    ##############################
    # Dirichlet boundary condition
    ##############################
    BC_u_1 = fe.DirichletBC(Vmix, -math.pi, left_point, method="pointwise")
    BC_u_2 = fe.DirichletBC(Vmix, math.pi, right_point, method="pointwise")
    #BC_u_3 = fe.DirichletBC(Vmix.sub(1), -r_init, left_point, method="pointwise")
    #BC_u_4 = fe.DirichletBC(Vmix.sub(1), r_init, right_point, method="pointwise")
    BC_u = [BC_u_1, BC_u_2]

    ##############################
    # Initialization
    ##############################
    if type == "lagrange":
        psi_fce = fe.interpolate(fe.Expression(("asin(0.9*x[0])"), r=r_init, degree=ex_deg), Vmix)
    else:
        raise Exception("Sorry, only Lagrange type implemented")

    fe.plot(psi_fce)
    plt.show()
    fe.plot(psi_fce.dx(0))
    plt.show()

    #if type == "lagrange":
        #xx, zz, r1, r2 = sol.split(deepcopy=True)
    #else:
    #    xx, zz = sol.split(deepcopy=True)
    #plt.plot(xx.vector()[:], zz.vector()[:])
    #plt.plot(-xx.vector()[:], zz.vector()[:])
    #plt.show()

    ##############################
    # Variational form
    ##############################
    x = fe.SpatialCoordinate(mesh)
    en = (0.5*ks*(fe.cos(psi_fce)*psi_fce.dx(0) + fe.sin(psi_fce)/x[0])**2 + kb*fe.cos(psi_fce)*fe.sin(psi_fce)*psi_fce.dx(0)/x[0] + 1)*2*math.pi*x[0]/fe.cos(psi_fce)*fe.dx

    print("E: ", fe.assemble(en))

    ##############################
    # Nonlinear solver
    ##############################
    d_en = fe.derivative(en, psi_fce, fe.TestFunction(Vmix))
    Jac = fe.derivative(d_en, psi_fce, fe.TrialFunction(Vmix))
    problem = fe.NonlinearVariationalProblem(d_en, psi_fce, BC_u, Jac)
    solver = fe.NonlinearVariationalSolver(problem)
    prm = solver.parameters
    # prm['newton_solver']['krylov_solver']['nonzero_initial_guess'] = True
    parameters = {"newton_solver": {"error_on_nonconvergence": False, \
                                    "relative_tolerance": 1e-6,
                                    "maximum_iterations": newton_iters}}
    solver.parameters.update(parameters)
    # print(fe.print_optimization)

    # solver_parameters={"newton_solver": {"linear_solver": "gmres",
    #                    "preconditioner": "ilu"}}
    # solver.parameters.update(solver_parameters)

    solver.solve()

    fe.plot(psi_fce)
    plt.show()

    #print("Vact: ", (fe.assemble(V_act * fe.dx) - V_s / math.pi * math.pi) * 10e12)
    print("E: ", fe.assemble(en))
    # print("En", fe.assemble((r_fce/math.pi - V_act)*fe.dx))
    # print(r_fce)

lipid_transformation_solver(type="lagrange")

import quadprog

#l_d2 = (x_fce.dx(0))**2 + (z_fce.dx(0))**2
    #l_d = fe.sqrt(l_d2)
    #psi_d = (-x_fce.dx(0).dx(0)*z_fce.dx(0) + x_fce.dx(0)*z_fce.dx(0).dx(0))/l_d2
    #psi_d = (-(x_init_fce.dx(0)).dx(0)*z_init_fce.dx(0) + x_init_fce.dx(0)*(z_init_fce.dx(0)).dx(0))/l_d2
    #H2 = psi_d/l_d + z_fce.dx(0)/(x_fce*l_d)

    #d_area = 2*d*H2*2*math.pi*x_fce*l_d
    #V_act = 0.5*fe.assemble(math.pi*x_init_fce**2*fe.sqrt(z_init_fce.dx(0)*z_init_fce.dx(0))*fe.dx)
    #V_act = math.pi*fe.dot(x_fce, x_fce)*z_fce.dx(0)

    #Energy_el = 0.5*kb*H2**2*2*math.pi*x_fce*l_d*fe.dx
    #if type == "lagrange":
        #Energy_el += r_fce*(V_act/V_s - fe.Constant(1/math.pi))*fe.dx
        #Energy_el += 0.5*kb*(d_area - fe.Constant(d_area_s))**2*fe.dx
    #    Energy_el += r_fce_1*2*math.pi*x_fce*l_d*fe.dx
    #    Energy_el += r_fce_2*math.pi*x_fce**2*z_fce.dx(0)*fe.dx
    #else:
        #Energy_el += fe.Constant(10e9)*(V_act/V_s - fe.Constant(1/math.pi))*fe.dx
    #    Energy_el += 0.5 * kb * (d_area/d_area_s) ** 2 * fe.dx
                    #+ 10e7*(r_fce/math.pi - V_act)**2*fe.dx
        #Energy_el = 0.5*kb*(2*curv_fce)**2*2*math.pi*x_init_fce*l_d*fe.dx + (r_fce/math.pi - V_act)*fe.dx
                    #+ r_fce*(V_act - V_s)**2*fe.dx
    #t_test = fe.TestFunction(Vmix)
    #n = fe.FacetNormal(mesh)
    #h = fe.CellDiameter(mesh)
    #h_avg = (h('+') + h('-')) / 2.0
    # Penalty parameter
    #alpha = fe.Constant(8.0)
    #d_en = fe.derivative(Energy_el, sol, t_test)
    #d_en -= fe.inner(fe.avg(fe.div(fe.grad(sol))), fe.jump(fe.grad(t_test), n))*fe.dS
    #d_en -= fe.inner(fe.jump(fe.grad(sol), n), fe.avg(fe.div(fe.grad(t_test))))*fe.dS
    #d_en += alpha/h_avg*fe.inner(fe.jump(fe.grad(sol), n), fe.jump(fe.grad(t_test), n))*fe.dS
    #d_vol_1 = fe.derivative(V_act, , t_test)
    #d_vol_2 = fe.derivative(V_act, sol, t_test)

