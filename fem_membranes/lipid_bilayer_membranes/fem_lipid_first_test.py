# -------------------------
# fem_lipid_first_test.py
# -------------------------

# -------------------------
# Description:
# - Based on article Finite element modeling of lipid bilayer membranes
#
# Last edit: 18.04. 2023
# -------------------------

import meshzoo
import fenics as fe
import matplotlib.pyplot as plt
import mpi4py
import mshr
import mshr as mr
import ufl
import math
import numpy as np

def test_01():
    # Spherical mesh from mshr package
    sphere = mr.Sphere(fe.Point(0.0, 0.0, 0.0), 1.0)
    mmesh = mr.generate_mesh(sphere, 20)
    mesh = fe.BoundaryMesh(mmesh, 'exterior')

    # Mesh ploting
    fe.plot(mesh)
    plt.show()

    mvc = fe.MeshValueCollection("size_t", mesh, 1)
    #with fe.XDMFFile(filename.replace(".", "_facet_region.")) as infile:
    #    infile.read(mvc, "name_to_read")
    facets = fe.MeshFunction("size_t", mesh, mvc)

    def local_frame(mesh):
        t = ufl.Jacobian(mesh)
        if mesh.geometric_dimension() == 2:
            t1 = fe.as_vector([t[0, 0], t[1, 0], 0])
            t2 = fe.as_vector([t[0, 1], t[1, 1], 0])
        else:
            t1 = fe.as_vector([t[0, 0], t[1, 0], t[2, 0]])
            t2 = fe.as_vector([t[0, 1], t[1, 1], t[2, 1]])
        e3 = fe.cross(t1, t2)
        e3 /= fe.sqrt(fe.dot(e3, e3))
        ey = fe.as_vector([0, 1, 0])
        ez = fe.as_vector([0, 0, 1])
        e1 = fe.cross(ey, e3)
        norm_e1 = fe.sqrt(fe.dot(e1, e1))
        e1 = fe.conditional(fe.lt(norm_e1, 0.5), ez, e1 / norm_e1)

        e2 = fe.cross(e3, e1)
        e2 /= fe.sqrt(fe.dot(e2, e2))
        return e1, e2, e3


    frame = local_frame(mesh)
    e1, e2, e3 = frame

    VT = fe.VectorFunctionSpace(mesh, "DG", 1, dim=3)

    ffile = fe.XDMFFile("output.xdmf")
    ffile.parameters["functions_share_mesh"] = True
    for (i, ei) in enumerate(frame):
        ei = fe.Function(VT, name="e{}".format(i + 1))
        ei.assign(fe.project(frame[i], VT))
        ffile.write(ei, 0)

    fe.plot(frame[2])
    plt.show()

    mesh = fe.SphericalShellMesh.create(mpi4py.MPI.COMM_WORLD, 10)
    fe.plot(mesh)
    plt.show()


def test_02():
    # Spherical mesh from mshr package
    sphere = mr.Sphere(fe.Point(0.0, 0.0, 0.0), 1.0)
    mmesh = mr.generate_mesh(sphere, 5)
    mesh = fe.BoundaryMesh(mmesh, 'exterior')

    # Mesh ploting
    fe.plot(mesh)
    plt.show()

    r = 1.0

    xs = fe.SpatialCoordinate(mesh)
    V = fe.VectorFunctionSpace(mesh, "CG", 3)
    # x = project(as_vector((xs[0],xs[1],xs[2])), V)  # does not work
    x = fe.project(fe.as_vector((xs[0], xs[1], xs[2])) / fe.sqrt(fe.dot(xs, xs)) * r, V)  # works
    n = fe.project(fe.as_vector((xs[0], xs[1], xs[2])) / fe.sqrt(fe.dot(xs, xs)), V)
    T = fe.FunctionSpace(mesh, "DG", 1)
    print(fe.project(0.5 * fe.dot(fe.div(fe.grad(x)), n), T).vector().get_local())

    kc = 1.0
    kg = 1.0
    c0 = 0.0
    mus = 1.0
    muv = 1.0
    H = 0.5 * fe.dot(fe.div(fe.grad(x)), n)
    K = 0.5 * fe.dot(fe.div(fe.grad(x)), n)

    # Energy
    E = (0.5*kc*(2*H - c0)**2 + kg*K)*fe.sqrt(fe.dot(xs, xs))*fe.dx
    E += 0.5*mus*(fe.sqrt(fe.dot(xs, xs)))**2*fe.dx
    E += 0.5*muv*(fe.sqrt(fe.dot(xs, xs)))**2*fe.dx


def test_03():
    def plot_shell(y, n=None):
        y_0, y_1, y_2 = y.split(deepcopy=True)
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.plot_trisurf(y_0.compute_vertex_values(),
                        y_1.compute_vertex_values(),
                        y_2.compute_vertex_values(),
                        triangles=y.function_space().mesh().cells(),
                        linewidth=1, antialiased=True, shade=False)
        if n:
            n_0, n_1, n_2 = n.split(deepcopy=True)
            ax.quiver(y_0.compute_vertex_values(),
                      y_1.compute_vertex_values(),
                      y_2.compute_vertex_values(),
                      n_0.compute_vertex_values(),
                      n_1.compute_vertex_values(),
                      n_2.compute_vertex_values(),
                      length=.2, color="r")
        ax.view_init(elev=20, azim=80)
        plt.xlabel(r"$x_0$")
        plt.ylabel(r"$x_1$")
        plt.xticks([-1, 0, 1])
        plt.yticks([0, math.pi / 2])
        return ax

    def director(beta):
        return fe.as_vector([fe.sin(beta[1]) * fe.cos(beta[0]), -fe.sin(beta[0]), fe.cos(beta[1]) * fe.cos(beta[0])])

    # Spherical mesh from mshr package
    sphere = mr.Sphere(fe.Point(0.0, 0.0, 0.0), 1.0)
    mmesh = mr.generate_mesh(sphere, 5)
    mesh = fe.BoundaryMesh(mmesh, 'exterior')

    fe.plot(mesh)
    plt.show()

    xs = fe.SpatialCoordinate(mesh)
    V = fe.VectorFunctionSpace(mesh, "CG", 3)
    # x = project(as_vector((xs[0],xs[1],xs[2])), V)  # does not work
    x = fe.project(fe.as_vector((xs[0], xs[1], xs[2])), V)

    d0 = director(x)
    V_normal = fe.FunctionSpace(mesh, "P", 1)

    print(mesh.ufl_cell())

    V_normal = fe.FunctionSpace(mesh, fe.VectorElement("P", fe.triangle, degree=1, dim=3))

    plot_shell(mesh, fe.project(d0, V_normal))


def test_05():
    domain = mr.Circle(fe.Point(0, 0), 2)
    mesh = mr.generate_mesh(domain, 10)

    mesh = fe.BoundaryMesh(mesh, 'exterior')
    print('PI is ', sum(cell.volume() for cell in fe.cells(mesh))/2)

    V = fe.FunctionSpace(mesh, "P", 1)
    fe.Constant(5.0)
    u_d = fe.Expression("x[0]", t=0.0, degree=1)
    u_d = fe.interpolate(u_d, V)

    filee = fe.File("mesh.pvd")
    filee << mesh
    fileee = fe.File("function.pvd")
    fileee << u_d
    fileee = fe.File("function_d1.pvd")
    fileee << fe.project(u_d.dx(1), V)
    fileee = fe.File("function_d2.pvd")
    fileee << fe.project(fe.grad(u_d)[0], V)

    #fe.plot(fe.project(fe.Constant(5.0), V))
    #plt.show()

    fe.plot(mesh)
    plt.show()


def test_06():
    t0 = 0
    num_elements = 300
    p_lbd = 2
    dt = 0.001

    # define periodic boundary
    class PeriodicBoundary(fe.SubDomain):

        def inside(self, x, on_boundary):
            #return bool(x[0] < fe.DOLFIN_EPS or x[0] > 1 - fe.DOLFIN_EPS and on_boundary)
            return bool(- fe.DOLFIN_EPS < x[0] < fe.DOLFIN_EPS and on_boundary)

        def map(self, x, y):
            y[0] = x[0] - 1
            print(x)

    mesh = fe.UnitIntervalMesh(num_elements)
    V = fe.FunctionSpace(mesh, 'P', 1, constrained_domain=PeriodicBoundary())

    u = fe.Function(V)
    v = fe.TestFunction(V)

    u0 = fe.Expression("p_lbd + sin(DOLFIN_PI*(2.2*x[0]))", p_lbd=p_lbd, element=V.ufl_element())

    u0 = fe.project(u0, V)

    # implicit euler formulation
    F = u * v * fe.dx + dt * fe.inner(fe.grad(u), fe.grad(v)) * u * fe.dx + dt * 0.01 * fe.inner(fe.grad(u), fe.grad(v)) * fe.dx - u0 * v * fe.dx

    T = 0.1
    t = dt

    # steps
    while t <= T:
        fe.solve(F == 0, u)
        t += dt
        u0.assign(u)

    #fe.plot(u)
    #plt.show()
    plt.plot(V.tabulate_dof_coordinates()[:, 0], u.vector().get_local())
    print(V.tabulate_dof_coordinates()[:, 0])
    print(u.vector().get_local())
    plt.show()

test_06()
