# -------------------------
# ode_lipid_parametrization_01.py
# -------------------------

# -------------------------
# Description:
# - Based on article Local sensitivity analysis of the “membrane shape equation” derived from the Helfrich energy
# - Version dimensionless area formulations
# - ODEs are solved by SciPy - solve_bvp
#
# Last edit: 04.05. 2023
# -------------------------

import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
import scipy.misc
from scipy.integrate import solve_bvp
import math


def c1(alp):
    return -0.5*C0*R0*((alp - alp0)/alp0)*(1.0 - math.tanh(gamma*(alp - alp0)))


def c2(ti):
    #return 0.5*C0*R0*(1.0 - np.tanh(gamma*(ti - t0)))
    return 0.0


def F2(ti):
    return (1.0 - np.tanh(gamma*(ti - t0)))


def dc(ti):
    d = scipy.misc.derivative(c2, ti)
    #return d
    return 0.0


def area_form(x, y):
    "right hand side of ODEs"
    dx = math.cos(y[2])/y[0]
    dy = math.sin(y[2])/y[0]
    dpsi = (2*y[0]*y[3] - math.sin(y[2]))/y[0]**2
    dh = y[4] + y[0]**2*dc
    dl = 2*y[3]*((y[3]-c2(x))**2 + y[5]/k_tilda) - 2*(y[3]-c2(x))*(y[3]**2 + (y[3] - math.sin(y[2])/y[0])**2)
    dlam = 2*k_tilda*(y[3] - c2(x))*dc

    return np.vstack([y[1,:], np.zeros(y.shape[1])]) # du/dx = p, dp/dx = 0


def area_form_mod(x, y):
    "right hand side of ODEs"
    dx = np.cos(y[2])
    dy = np.sin(y[2])
    dpsi = 2*y[3] - np.sin(y[2])/y[0]
    print(dc(0))
    dh = y[4]/y[0] + dc(x)
    dl = 2*y[0]*y[3]*((y[3]-c2(x))**2 + y[5]/k_tilda) - 2*y[0]*(y[3]-c2(x))*(y[3]**2 + (y[3] - np.sin(y[2])/y[0])**2)
    dlam = 2*k_tilda*(y[3] - c2(x))*dc(x)

    return np.vstack([dx, dy, dpsi, dh, dl, dlam])  # du/dx = p, dp/dx = 0


def bc(ya, yb): # ya = y[:,0], yb = y[:,-1]
    "boundary condition residual"
    return np.array([ya[0] - 0.1, ya[2], ya[4], yb[1], yb[2], yb[5] - lam_tilda])


R0 = 200.0e-9
C0 = 0.004e-9
gamma = 30.0
alp0 = 30.0
s_start = 0.0
s_end = 40.0
t0 = 2.0
k_tilda = 0.1e3
lam_tilda = 12.8

ts = np.linspace(s_start, s_end, 1000)
y0 = np.random.randn(6,len(ts))
#y0 = np.ones((6, len(ts)))
Fs = [c2(tsi) for tsi in ts]

plt.plot(ts, Fs)
plt.show()

sol = solve_bvp(area_form_mod, bc, ts, y0)

plt.plot(sol.x, sol.y[3,:])
plt.xlabel('x')
plt.ylabel('y')
plt.show()

plt.plot(sol.y[0, :], sol.y[1, :])
plt.xlabel('x')
plt.ylabel('y')
plt.show()

#gamma = 2*math.pi*R0**2*xi
#alp0 = a0/(2*math.pi*R0**2)