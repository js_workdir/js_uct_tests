# -------------------------
# fem_lipid_first_test.py
# -------------------------
import fenics
# -------------------------
# Description:
# - Based on article Finite element modeling of lipid bilayer membranes
#
# Last edit: 23.04. 2023
# -------------------------

import meshzoo
import fenics as fe
import matplotlib.pyplot as plt
import mpi4py
import mshr
import mshr as mr
import ufl
import math
import numpy as np
import dolfin_adjoint as doa


##############################
# Parameters
##############################
num_elements = 100  # Number of elements
v_deg = 5
ex_deg = 100
r_init = 2.0e-6  # Radius
#r_init = 2
ks = 0.1  # Material parameter
kb = 0.1  # Material parameter
d = 1.2e-9  # Half thickness of membrane
#d = 0.002
rho_i_rho_0 = 1.0
rho_o_rho_0 = 1.0
d_area_s = 16*math.pi*d*r_init
V_s = 4/3*math.pi*r_init**3
ddV_pre = 1.0
ddA_pre = 1.0
pen1 = 1.0e5
pen2 = 1.0e5
tol = 1.0e-14
cstrns = "pen"
newton_iters = 30
#cstrns = "lagrange"
#cstrns = "lagrange2"


##############################
# Mesh with periodic boundary
##############################
# define periodic boundary
class PeriodicBoundary(fe.SubDomain):
    def inside(self, x, on_boundary):
        # return bool(x[0] < fe.DOLFIN_EPS or x[0] > 1 - fe.DOLFIN_EPS and on_boundary)
        #return bool(- fe.DOLFIN_EPS < x[0] < fe.DOLFIN_EPS and on_boundary)
        return on_boundary

    #def map(self, x, y):
    #    y[0] = x[0] - math.pi

    def map(self, x, y):
        if fe.near(x[0], math.pi):
            y[0] = x[0] - math.pi
        else:
            y[0] = x[0]


# Definition of supporting point
def left_point(x):
    return fe.near(x[0], 0.0)


# Definition of loading point
def right_point(x):
    return fe.near(x[0], math.pi)


# Mesh constructor
mesh = fe.IntervalMesh(num_elements, 0.0, math.pi)

##############################
# Function space
##############################
V = fe.FunctionSpace(mesh, "CG", v_deg)
P1 = fe.FiniteElement('P', fe.interval, v_deg)
if cstrns == "lagrange":
    R1 = fe.FiniteElement("Real", fe.interval, 0)
    element = fe.MixedElement([P1, P1, R1])
elif cstrns == "lagrange2":
    element = fe.MixedElement([P1, P1, P1])
else:
    element = fe.MixedElement([P1, P1])
Vmix = fe.FunctionSpace(mesh, element)
sol = fe.Function(Vmix)
#sol2 = fe.MixedFunctionSpace()

#u_d = fe.Expression("-t", t=0.0, degree=0)  # Prescribed displacement
#u_d_2 = fe.Expression("t", t=0.0, degree=0)  # Prescribed displacement
BC_u_1 = fe.DirichletBC(Vmix.sub(0), 0.0, left_point, method="pointwise")
BC_u_2 = fe.DirichletBC(Vmix.sub(0), 0.0, right_point, method="pointwise")
BC_u_3 = fe.DirichletBC(Vmix.sub(1), -r_init, left_point, method="pointwise")
BC_u_4 = fe.DirichletBC(Vmix.sub(1), r_init, right_point, method="pointwise")
BC_u = [BC_u_1, BC_u_2, BC_u_3]

##############################
# Initialization
##############################
x_init = fe.Expression("r*sin(x[0])", r=r_init, degree=4)
z_init = fe.Expression("-r*cos(x[0])", r=r_init, degree=4)
x_init_fce_i = fe.interpolate(x_init, V)
z_init_fce_i = fe.interpolate(z_init, V)

if cstrns == "lagrange":
    sol = fe.interpolate(fe.Expression(("r*sin(x[0])", "-r*cos(x[0])", "0"), r=r_init, degree=ex_deg), Vmix)
    x_init_fce, z_init_fce, r_fce = fe.split(sol)
elif cstrns == "lagrange2":
    sol = fe.interpolate(fe.Expression(("r*sin(x[0])", "-r*cos(x[0])", "1"), r=r_init, degree=ex_deg), Vmix)
    x_init_fce, z_init_fce, lam_fce = fe.split(sol)
else:
    sol = fe.interpolate(fe.Expression(("r*sin(x[0])", "-1.1*r*cos(x[0])"), r=r_init, degree=ex_deg), Vmix)
    #fe.Expression("(r*r - (-r + 2*x[0]*r)*(-r + 2*x[0]*r))", r=r_init, degree=4)
    #sol = fe.interpolate(fe.Expression(("sqrt(r*r - (-r + 2*x[0]*r)*(-r + 2*x[0]*r))", "-r + 2*x[0]*r"), r=r_init, degree=4), Vmix)
    x_init_fce, z_init_fce = fe.split(sol)

if cstrns == "lagrange":
    xx, zz, rr = sol.split(deepcopy=True)
else:
    xx, zz = sol.split(deepcopy=True)
plt.plot(xx.vector()[:], zz.vector()[:])
plt.plot(-xx.vector()[:], zz.vector()[:])
plt.show()

##############################
# Variational form
##############################
l_d2 = (x_init_fce.dx(0))**2 + (z_init_fce.dx(0))**2
l_d = fe.sqrt(l_d2)
#print(r_init)
#fe.plot(l_d)
#plt.show()
#l_d = fe.interpolate(fe.Constant(r_init), V)
#fe.plot(l_d)
#plt.ylim(1e-6, 3e-6)
#bottom, top = plt.ylim()  # return the current ylim
#plt.ylim((bottom, top))   # set the ylim to bottom, top
#plt.ylim(bottom, top)     # set the ylim to bottom, top
#plt.show()
#l_d2 = l_d*l_d
psi_d = (-(x_init_fce.dx(0)).dx(0)*z_init_fce.dx(0) + x_init_fce.dx(0)*(z_init_fce.dx(0)).dx(0))/l_d2
curv_fce = (psi_d/l_d + z_init_fce.dx(0)/(x_init_fce*l_d))

#Energy_el = (0.5*kb*2*curv_fce**2 + 0.5*ks*((rho_i_rho_0 - 1.0 - 2*d*curv_fce)**2
#                                         + (rho_o_rho_0 - 1.0 - 2*d*curv_fce)**2))*2*math.pi*x_init_fce*l_d*fe.dx




d_area = 2*d*curv_fce*2*math.pi*x_init_fce*l_d
#V_act = 0.5*fe.assemble(math.pi*x_init_fce**2*fe.sqrt(z_init_fce.dx(0)*z_init_fce.dx(0))*fe.dx)
V_act = math.pi*x_init_fce**2*z_init_fce.dx(0)

if cstrns == "lagrange":
    Energy_el = 0.5*kb*curv_fce**2*2*math.pi*x_init_fce*l_d*fe.dx
                #+ 10e7*(r_fce/math.pi - V_act)**2*fe.dx
    #Energy_el = 0.5*kb*(2*curv_fce)**2*2*math.pi*x_init_fce*l_d*fe.dx + (r_fce/math.pi - V_act)*fe.dx
                #+ r_fce*(V_act - V_s)**2*fe.dx
elif cstrns == "lagrange2":
    Energy_el = 0.5*kb*curv_fce**2*2*math.pi*x_init_fce*l_d*fe.dx
    #Energy_el += lam_fce*(V_act - fe.Constant(V_s)/math.pi)*fe.dx
else:
    Energy_el = 0.5*kb*(curv_fce)**2*2*math.pi*x_init_fce*l_d*fe.dx
    #Energy_el = pen1*(V_act/V_s - fe.Constant(1/math.pi))*fe.dx
    #vyr = V_act/V_s - fe.Constant(1/math.pi)
    #Energy_el += pen1*fe.conditional(fe.gt(vyr, 0), vyr, -vyr)*fe.dx
    #Energy_el += pen2*(d_area/d_area_s - fe.Constant(1/math.pi))**2*fe.dx
                #+ 10e10*V_act**2*fe.dx
    #Energy_el = pen1*(fe.Constant(fe.assemble(V_act)/V_s) - fe.Constant(1/math.pi))*fe.dx
    Vact = fe.assemble(V_act * fe.dx)
    Vact2 = fe.interpolate(fe.Constant(Vact / V_s), V)
    Energy_el += (Vact2 - fe.Constant(1)) * fe.dx
print(fe.assemble(Energy_el))

print(fenics.list_linear_solver_methods())
print(fenics.list_lu_solver_methods())

#J = fe.assemble(Energy_el)
#J_hat = doa.ReducedFunctional(J, doa.Control(fe.Function(Vmix)))
#u_opt = doa.minimize(J_hat, method="L-BFGS-B")



#Energy_el_2 = Energy_el + pen1*0.5*(V_act - V_s/math.pi)**2*fe.dx
#Energy_el_2 = Energy_el + 10e10*(V_act*fe.dx - fe.interpolate(fe.Constant(V_s), V)*fe.dx) + fe.assemble(V_act)*fe.dx
#pen1*0.5*(V_act - V_s/math.pi)**2*fe.dx
#print(fe.assemble(Energy_el_2))

print("Vact: ", fe.assemble(V_act*fe.dx))
print("Vs: ", V_s/math.pi*math.pi)
#print("En", fe.assemble((r_fce/math.pi - V_act)*fe.dx))
#print("En", fe.assemble(r_fce*(fe.Constant(1.0)/math.pi - V_act)*fe.dx))

#Energy_final = Energy_el + 0.5*pen1*(d_area - d_area_s*ddA_pre)**2*fe.dx + 0.5*pen2*(V_act - V_s*ddV_pre)**2*fe.dx + 5
               #+ 0.5*pen2*(V_act*fe.dx - V_s*ddV_pre*fe.dx)**2
#print(fe.assemble(Energy_final))

##############################
# Nonlinear solver
##############################
#sol.vector()[:] = np.random.rand(sol.vector().size())
ttest = fe.TestFunction(Vmix)
Form = fe.derivative(Energy_el, sol, ttest)
#vact = fe.derivative(V_act*fe.dx, sol, ttest)
#Form1 = V_act*ttest*fe.dx
#Form_2 = Form + (V_act - fe.Constant(V_s))*ttest*fe.dx
#print(np.sum(fe.assemble(fe.Constant(V_s)*ttest[0]*fe.dx)[:]))
#Form_2 = Form + (vact) + fe.Constant(V_s)*ttest[0]*fe.dx
Jac = fe.derivative(Form, sol, fe.TrialFunction(Vmix))
problem = fe.NonlinearVariationalProblem(Form, sol, BC_u, Jac)
solver = fe.NonlinearVariationalSolver(problem)
prm = solver.parameters
prm['newton_solver']['krylov_solver']['nonzero_initial_guess'] = True
parameters = {"newton_solver": {"error_on_nonconvergence": False,\
                                            "relative_tolerance": 1e-6,
                                "maximum_iterations": newton_iters}}
solver.parameters.update(parameters)
#print(fe.print_optimization)

#solver_parameters={"newton_solver": {"linear_solver": "gmres",
#                    "preconditioner": "ilu"}}
#solver.parameters.update(solver_parameters)

solver.solve()
print("En ", fe.assemble(Energy_el))
#print(fe.assemble(Energy_el_2))
#print(np.amax(sol.vector()[:]))

fe.plot(sol.sub(0))
plt.show()
fe.plot(sol.sub(1))
plt.show()

print("Vact: ", (fe.assemble(V_act*fe.dx) - V_s/math.pi*math.pi)*10e12)
#print("En", fe.assemble((r_fce/math.pi - V_act)*fe.dx))
#print(r_fce)

#fe.plot(r_fce)
#plt.show()
print(fe.assemble(d_area*fe.dx))
print(d_area_s)
print(fe.assemble(d_area*fe.dx)/d_area_s)
print(fe.assemble(V_act*fe.dx)/V_s)

if cstrns == "lagrange":
    xx, zz, rr = sol.split(deepcopy=True)
else:
    xx, zz = sol.split(deepcopy=True)
plt.plot(xx.vector()[:], zz.vector()[:])
plt.show()
