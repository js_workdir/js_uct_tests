# -------------------------
# fem_lipid_1D_model_test.py
# -------------------------

# -------------------------
# Description:
# - Based on article Finite element modeling of lipid bilayer membranes
#
# Last edit: 25.04. 2023
# -------------------------

import meshzoo
import fenics as fe
import matplotlib.pyplot as plt
import mpi4py
import mshr
import mshr as mr
import ufl
import math
import numpy as np


def energy_calc(radius, dd, penal, shapes=["r*sin(x[0])", "-r*cos(x[0])"]):
    ##############################
    # Parameters
    ##############################
    num_elements = 200  # Number of elements
    v_deg = 5
    ex_deg = 100
    # r_init = 2.0e-6  # Radius
    # r_init = 2
    r_init = radius
    ks = 0.1  # Material parameter
    kb = 0.1  # Material parameter
    #d = 1.2e-9  # Half thickness of membrane
    d = dd
    # d = 0.002
    d_area_s = 16 * math.pi * d * r_init
    V_s = 4 / 3 * math.pi * r_init ** 3
    ddV_pre = 1.0
    ddA_pre = 1.0
    pen1 = 1.0e5
    pen2 = 1.0e5
    tol = 1.0e-14
    cstrns = "pen"
    newton_iters = 150
    # cstrns = "lagrange"
    # cstrns = "lagrange2"

    ##############################
    # Mesh with periodic boundary
    ##############################
    # Definition of supporting point
    def left_point(x):
        return fe.near(x[0], 0.0)


    # Definition of loading point
    def right_point(x):
        return fe.near(x[0], math.pi)


    # Mesh constructor
    mesh = fe.IntervalMesh(num_elements, 0.0, math.pi)

    ##############################
    # Function space
    ##############################
    V = fe.FunctionSpace(mesh, "CG", v_deg)
    P1 = fe.FiniteElement('P', fe.interval, v_deg)
    if cstrns == "lagrange":
        R1 = fe.FiniteElement("Real", fe.interval, 0)
        element = fe.MixedElement([P1, P1, R1])
    elif cstrns == "lagrange2":
        element = fe.MixedElement([P1, P1, P1])
    else:
        element = fe.MixedElement([P1, P1])
    Vmix = fe.FunctionSpace(mesh, element)
    sol = fe.Function(Vmix)
    #sol2 = fe.MixedFunctionSpace()

    #u_d = fe.Expression("-t", t=0.0, degree=0)  # Prescribed displacement
    #u_d_2 = fe.Expression("t", t=0.0, degree=0)  # Prescribed displacement
    BC_u_1 = fe.DirichletBC(Vmix.sub(0), 0.0, left_point, method="pointwise")
    BC_u_2 = fe.DirichletBC(Vmix.sub(0), 0.0, right_point, method="pointwise")
    BC_u_3 = fe.DirichletBC(Vmix.sub(1), -r_init, left_point, method="pointwise")
    BC_u_4 = fe.DirichletBC(Vmix.sub(1), r_init, right_point, method="pointwise")
    BC_u = [BC_u_1, BC_u_2, BC_u_3, BC_u_4]

    ##############################
    # Initialization
    ##############################

    if cstrns == "lagrange":
        sol = fe.interpolate(fe.Expression(("r*sin(x[0])", "-r*cos(x[0])", "0"), r=r_init, degree=ex_deg), Vmix)
        x_init_fce, z_init_fce, r_fce = fe.split(sol)
    elif cstrns == "lagrange2":
        sol = fe.interpolate(fe.Expression(("r*sin(x[0])", "-r*cos(x[0])", "1"), r=r_init, degree=ex_deg), Vmix)
        x_init_fce, z_init_fce, lam_fce = fe.split(sol)
    else:
        sol = fe.interpolate(fe.Expression((shapes[0], shapes[1]), r=r_init, degree=ex_deg), Vmix)
        #fe.Expression("(r*r - (-r + 2*x[0]*r)*(-r + 2*x[0]*r))", r=r_init, degree=4)
        #sol = fe.interpolate(fe.Expression(("sqrt(r*r - (-r + 2*x[0]*r)*(-r + 2*x[0]*r))", "-r + 2*x[0]*r"), r=r_init, degree=4), Vmix)
        x_init_fce, z_init_fce = fe.split(sol)

    if cstrns == "lagrange":
        xx, zz, rr = sol.split(deepcopy=True)
    else:
        xx, zz = sol.split(deepcopy=True)
    plt.plot(xx.vector()[:], zz.vector()[:])
    plt.plot(-xx.vector()[:], zz.vector()[:])
    plt.show()

    ##############################
    # Variational form
    ##############################
    l_d2 = (x_init_fce.dx(0))**2 + (z_init_fce.dx(0))**2
    l_d = fe.sqrt(l_d2)
    psi_d = (-(x_init_fce.dx(0)).dx(0)*z_init_fce.dx(0) + x_init_fce.dx(0)*(z_init_fce.dx(0)).dx(0))/l_d2
    curv_fce = (psi_d/l_d + z_init_fce.dx(0)/(x_init_fce*l_d))
    d_area = 2*d*curv_fce*2*math.pi*x_init_fce*l_d
    V_act = math.pi*x_init_fce**2*z_init_fce.dx(0)

    Energy_el = 0.5*kb*curv_fce**2*2*math.pi*x_init_fce*l_d*fe.dx
    if penal > 0:
        #vyr = V_act/V_s - fe.Constant(1/math.pi)
        #Energy_el += pen1 * fe.conditional(fe.gt(vyr, 0), vyr, -vyr) * fe.dx
        Vact = fe.assemble(V_act*fe.dx)
        Vact2 = fe.interpolate(fe.Constant(Vact/V_s), V)
        Energy_el += (Vact2 - fe.Constant(1))*fe.dx
        #Energy_el += pen1 * fe.interpolate(fe.Constant(fe.assemble(V_act*fe.dx) / V_s) - fe.Constant(1 / math.pi), V) * fe.dx
        #Energy_el += pen1*(V_act/V_s - fe.Constant(1/math.pi))*fe.dx
        #Energy_el += pen2*(d_area/d_area_s - fe.Constant(1/math.pi))*fe.dx
    print("######")
    print("Energy is ", fe.assemble(Energy_el)/8.0/math.pi/0.1)
    print("V/Vs is ", fe.assemble(V_act*fe.dx)/V_s)
    print("A/As is ", fe.assemble(d_area*fe.dx)/d_area_s)
    print("Test ", fe.assemble((d_area/d_area_s - fe.Constant(1/math.pi))*fe.dx))
    print("Test ", fe.assemble((V_act/V_s - fe.Constant(1/math.pi))*fe.dx))
    print("Test ", fe.assemble((V_act - fe.Constant(V_s))**2*fe.dx))
    vyr = (V_act / V_s - fe.Constant(1 / math.pi))
    vyr2 = (V_act / V_s - fe.Constant(1 / math.pi))*fe.dx
    vyr3 = fe.conditional(fe.gt(vyr, 0), vyr, -vyr)
    print("Test2 ", fe.assemble(vyr2))
    print("Test2 ", fe.assemble(vyr3*fe.dx))

    #aa = fe.assemble(V_act*fe.dx)
    #fe.plot(fe.interpolate(fe.Constant(aa), V))
    #fe.plot(vyr)
    #fe.plot(vyr3)
    #plt.show()


energy_calc(2.0e-6, 1.2e-9, 0, shapes=["r*sin(x[0])", "-r*cos(x[0])"])
energy_calc(2.0e-6, 1.2e-9, 0, shapes=["r*sin(x[0]) + 0.2*r*sin(2.9*x[0])", "-r*cos(x[0])"])
energy_calc(2.0e-6, 1.2e-9, 1, shapes=["r*sin(x[0])", "-r*cos(x[0])"])
energy_calc(2.0e-6, 1.2e-9, 1, shapes=["r*sin(x[0]) + 0.2*r*sin(2.9*x[0])", "-r*cos(x[0])"])
