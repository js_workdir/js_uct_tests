# -------------------------
# fem_lipid_first_test.py
# -------------------------

# -------------------------
# Description:
# - Based on article Finite element modeling of lipid bilayer membranes
#
# Last edit: 18.04. 2023
# -------------------------

import meshzoo
import fenics as fe
import matplotlib.pyplot as plt
import mpi4py
import mshr
import mshr as mr
import ufl
import math
import numpy as np


##############################
# Parameters
##############################
num_elements = 60
r_init = 5
ks = 1.0
kb = 1.0
d = 0.02
rho_i_rho_0 = 1.0
rho_o_rho_0 = 1.0
d_area_s = 16*math.pi*d*r_init
V_s = 4/3*math.pi*r_init**3

ddV_pre = 1.0
ddA_pre = 1.0

pen1 = 1.0e6
pen2 = 1.0e6

##############################
# Mesh with periodic boundary
##############################
#mesh = fe.IntervalMesh(num_elements, 0, 2*math.pi)
#mesh = fe.IntervalMesh(num_elements, 0.0, 1.0)


tol = 1.0e-14
left_part = fe.AutoSubDomain(lambda x: x[0] <= 0.5*math.pi or x[0] >= 1.5*math.pi)
#left_part = fe.AutoSubDomain(lambda x: x[0] <= math.pi)
boundaries = fe.MeshFunction("size_t", mesh, mesh.topology().dim())
boundaries.set_all(0)
left_part.mark(boundaries, 1)
dx = fe.dx(subdomain_data=boundaries)

filee = fe.File("test.pvd")
filee << boundaries

V = fe.FunctionSpace(mesh, "CG", 3)

P1 = fe.FiniteElement('P', fe.interval, 3)
element = fe.MixedElement([P1, P1])
Vmix = fe.FunctionSpace(mesh, element)
sol = fe.Function(Vmix)
#x_init_fce, z_init_fce = fe.Function(Vmix)

#x_init_fce, z_init_fce = sol.split()

fa = fe.FunctionAssigner(Vmix, [V,V])
#s_init = fe.interpolate(fe.Constant(0.0), V_sca)
#v_init = fe.interpolate(fe.Constant((0.0, 0.0)), V_vec)
#r_init = fe.interpolate(fe.Constant(-dt * v0), V_rea)

#x_init.assign(fe.Expression("r*cos(x[0])", r=r_init, degree=4))
#z_init.assign()
#x_init = fe.Expression("r*cos(x[0])", p_lbd=p_lbd, element=V.ufl_element())
x_init = fe.Expression("r*cos(x[0])-r*sin(x[0]-1)", r=r_init, degree=4)
z_init = fe.Expression("r*sin(x[0])", r=r_init, degree=4)

#fa.assign(X_old2, [s_init] + v1_elems_num * [v_init] + [r_init])

#x_init_fce.assign(fe.interpolate(x_init, V))
#z_init_fce.assign(fe.interpolate(z_init, V))
x_init_fce = fe.interpolate(x_init, V)
z_init_fce = fe.interpolate(z_init, V)

fa.assign(sol, [x_init_fce, z_init_fce])

l_d2 = (x_init_fce.dx(0))**2 + (z_init_fce.dx(0))**2
l_d = fe.sqrt(l_d2)
psi_d = (-x_init_fce.dx(0).dx(0)*z_init_fce.dx(0) + x_init_fce.dx(0)*z_init_fce.dx(0).dx(0))/l_d2
curv_fce = 0.5*(psi_d/l_d + z_init_fce.dx(0)/(x_init_fce*l_d))

print(fe.assemble(math.pi*x_init_fce**2*fe.sqrt(z_init_fce.dx(0)*z_init_fce.dx(0))*fe.dx(0)))
print(4/3*math.pi*r_init**3)

#fe.plot(curv_fce)
fe.plot(x_init_fce)
plt.show()

Energy_el = (0.5*kb*2*curv_fce + 0.5*ks*((rho_i_rho_0 - 1.0 - 2*d*curv_fce)**2
                                         + (rho_o_rho_0 - 1.0 - 2*d*curv_fce)**2))*2*math.pi*x_init_fce*l_d*fe.dx
#Energy_el = 0.5*kb*2*curv_fce*fe.dx
#Energy_el = x_init_fce.dx(0)*fe.dx

d_area = 2*d*2*curv_fce*2*math.pi*x_init_fce*l_d
#V_act = 0.5*fe.assemble(math.pi*x_init_fce**2*fe.sqrt(z_init_fce.dx(0)*z_init_fce.dx(0))*fe.dx)
V_act = 0.5*math.pi*x_init_fce**2*fe.sqrt(z_init_fce.dx(0)*z_init_fce.dx(0))

fe.plot(x_init_fce)
fe.plot(sol.sub(0))
plt.show()


Energy_final = Energy_el + 0.5*pen1*(d_area - d_area_s*ddA_pre)**2*dx(1) + 0.5*pen2*(V_act - V_s*ddV_pre)**2*fe.dx
               #+ 0.5*pen2*(V_act*fe.dx - V_s*ddV_pre*fe.dx)**2
print(fe.assemble(Energy_final))
Form = fe.derivative(Energy_el, sol, fe.TestFunction(Vmix))
#Form_final = Form + pen1*(d_area*fe.dx - d_area_s*ddA_pre*fe.dx) + pen2*(V_act*fe.dx - V_s*ddV_pre*fe.dx)
Jac = fe.derivative(Form, sol, fe.TrialFunction(Vmix))
problem = fe.NonlinearVariationalProblem(Form, sol, [], Jac)
solver = fe.NonlinearVariationalSolver(problem)
prm = solver.parameters
prm['newton_solver']['krylov_solver']['nonzero_initial_guess'] = True
solver.solve()

print(np.amax(sol.vector()[:]))

fe.plot(sol.sub(1))
plt.show()

print(fe.assemble(d_area*dx(1)))
print(d_area_s)
print(fe.assemble(d_area*dx(1))/d_area_s)
print(fe.assemble(V_act*fe.dx)/V_s)
