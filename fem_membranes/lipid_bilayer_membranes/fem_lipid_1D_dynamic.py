# -------------------------
# fem_lipid_1D_dynamic.py
# -------------------------

# -------------------------
# Description:
# - Based on article Finite element modeling of lipid bilayer membranes
# - Version with lagrange multipliers
# - Dynamic version
#
# Last edit: 29.04. 2023
# -------------------------

import fenics
import fenics as fe
import matplotlib.pyplot as plt
import mpi4py
import ufl
import math
import numpy as np


def lipid_transformation_solver(type="lagrange"):
    ##############################
    # Parameters
    ##############################
    num_elements = 1000  # Number of elements
    v_deg = 1
    ex_deg = 100
    r_init = 2.0e-6  # Radius
    #r_init = 2
    ks = 0.1  # Material parameter
    kb = 0.1  # Material parameter
    d = 1.2e-9  # Half thickness of membrane
    #d = 0.002
    d_area_s = 16*math.pi*d*r_init
    V_s = 4/3*math.pi*r_init**3
    ddV_pre = 1.0
    ddA_pre = 1.0
    tol = 1.0e-14
    newton_iters = 30

    # Definition of start point
    def left_point(x):
        return fe.near(x[0], 0.0)

    # Definition of end point
    def right_point(x):
        return fe.near(x[0], math.pi)

    # Mesh constructor
    mesh = fe.IntervalMesh(num_elements, 0.0, math.pi)
    #mesh = fe.IntervalMesh(num_elements, 0.0, 1.0)

    ##############################
    # Function space
    ##############################
    V = fe.FunctionSpace(mesh, "CG", v_deg)
    P1 = fe.FiniteElement('P', fe.interval, v_deg)
    R1 = fe.FiniteElement("Real", fe.interval, 0)
    if type == "lagrange":
        element = fe.MixedElement([P1, P1, R1])
    else:
        element = fe.MixedElement([P1, P1])
    Vmix = fe.FunctionSpace(mesh, element)
    sol = fe.Function(Vmix)

    BC_u_1 = fe.DirichletBC(Vmix.sub(0), 0.0, left_point, method="pointwise")
    BC_u_2 = fe.DirichletBC(Vmix.sub(0), 0.0, right_point, method="pointwise")
    BC_u_3 = fe.DirichletBC(Vmix.sub(1), -r_init, left_point, method="pointwise")
    BC_u_4 = fe.DirichletBC(Vmix.sub(1), r_init, right_point, method="pointwise")
    BC_u = [BC_u_1, BC_u_2, BC_u_3]

    ##############################
    # Initialization
    ##############################
    #sol = fe.interpolate(fe.Expression(("r*sin(x[0])", "-r*cos(x[0])", "0"), r=r_init, degree=ex_deg), Vmix)
    #sol = fe.interpolate(fe.Expression(("sqrt(r*r - (-r + 2*x[0]*r)*(-r + 2*x[0]*r))", "-r + 2*x[0]*r", "1"), r=r_init, degree=4), Vmix)
    if type == "lagrange":
        #sol = fe.interpolate(fe.Expression(("r*sin(x[0]) + 0.2*r*sin(2.9*x[0])", "-r*cos(x[0])", "1e9"), r=r_init, degree=ex_deg), Vmix)
        sol = fe.interpolate(fe.Expression(("r*sin(x[0])", "-r*cos(x[0])", "0"), r=r_init, degree=ex_deg), Vmix)
        x_fce, z_fce, r_fce = fe.split(sol)
    else:
        #sol = fe.interpolate(fe.Expression(("r*sin(x[0]) + 0.2*r*sin(2.9*x[0])", "-r*cos(x[0])"), r=r_init, degree=ex_deg), Vmix)
        sol = fe.interpolate(fe.Expression(("r*sin(x[0])", "-r*cos(x[0])"), r=r_init, degree=ex_deg), Vmix)
        x_fce, z_fce = fe.split(sol)

    fe.plot(x_fce)
    plt.show()
    fe.plot(z_fce)
    plt.show()

    if type == "lagrange":
        xx, zz, rr = sol.split(deepcopy=True)
    else:
        xx, zz = sol.split(deepcopy=True)
    plt.plot(xx.vector()[:], zz.vector()[:])
    plt.plot(-xx.vector()[:], zz.vector()[:])
    plt.show()

    ##############################
    # Variational form
    ##############################
    # Unknowns
    xt = 0.0
    xdt = 0.0
    zdt = 0.0
    xddt = 0.0
    zddt = 0.0

    dA = 0.0
    dAt = 0.0

    rhoi = 0.0
    rhoo = 0.0

    zd = z_fce.dx(0)
    zdd = z_fce.dx(0).dx(0)
    xd = x_fce.dx(0)
    xdd = x_fce.dx(0).dx(0)
    l_d2 = (x_fce.dx(0))**2 + (z_fce.dx(0))**2
    l_d = fe.sqrt(l_d2)
    psi_d = (-x_fce.dx(0).dx(0)*z_fce.dx(0) + x_fce.dx(0)*z_fce.dx(0).dx(0))/l_d2
    H2 = psi_d/l_d + z_fce.dx(0)/(x_fce*l_d)

    H2t = -zd/(l_d*x_fce**2)*xt + (zdd - 3.0*psi_d*xd)/(l_d**3)*xdt
    H2t += -(xdd + 3.0*psi_d*zd)/(l_d**3)*zdt - zd/(l_d**3)*xddt + xd/(l_d**3)*zddt

    dE = 0.5*kb*H2*(2*H2t*dA + H2*dAt)
    dE += 0.5*ks*(rhoi/rhoo - 1.0 + 2)













    #psi_d = (-(x_init_fce.dx(0)).dx(0)*z_init_fce.dx(0) + x_init_fce.dx(0)*(z_init_fce.dx(0)).dx(0))/l_d2


    d_area = 2*d*H2*2*math.pi*x_fce*l_d
    #V_act = 0.5*fe.assemble(math.pi*x_init_fce**2*fe.sqrt(z_init_fce.dx(0)*z_init_fce.dx(0))*fe.dx)
    V_act = math.pi*fe.dot(x_fce, x_fce)*z_fce.dx(0)

    Energy_el = 0.5*kb*H2**2*2*math.pi*x_fce*l_d*fe.dx
    if type == "lagrange":
        #Energy_el += r_fce*(V_act/V_s - fe.Constant(1/math.pi))*fe.dx
        Energy_el += 0.5*kb*(d_area - fe.Constant(d_area_s))**2*fe.dx
    else:
        #Energy_el += fe.Constant(10e9)*(V_act/V_s - fe.Constant(1/math.pi))*fe.dx
        Energy_el += 0.5 * kb * (d_area/d_area_s) ** 2 * fe.dx
                    #+ 10e7*(r_fce/math.pi - V_act)**2*fe.dx
        #Energy_el = 0.5*kb*(2*curv_fce)**2*2*math.pi*x_init_fce*l_d*fe.dx + (r_fce/math.pi - V_act)*fe.dx
                    #+ r_fce*(V_act - V_s)**2*fe.dx
    t_test = fe.TestFunction(Vmix)
    d_en = fe.derivative(Energy_el, sol, t_test)
    #d_vol_1 = fe.derivative(V_act, , t_test)
    #d_vol_2 = fe.derivative(V_act, sol, t_test)

    print("E: ", fe.assemble(Energy_el))

    ##############################
    # Nonlinear solver
    ##############################
    Jac = fe.derivative(d_en, sol, fe.TrialFunction(Vmix))
    problem = fe.NonlinearVariationalProblem(d_en, sol, BC_u, Jac)
    solver = fe.NonlinearVariationalSolver(problem)
    prm = solver.parameters
    #prm['newton_solver']['krylov_solver']['nonzero_initial_guess'] = True
    parameters = {"newton_solver": {"error_on_nonconvergence": False,\
                                                "relative_tolerance": 1e-6,
                                    "maximum_iterations": newton_iters}}
    solver.parameters.update(parameters)
    #print(fe.print_optimization)

    #solver_parameters={"newton_solver": {"linear_solver": "gmres",
    #                    "preconditioner": "ilu"}}
    #solver.parameters.update(solver_parameters)

    solver.solve()

    fe.plot(sol.sub(0))
    plt.show()
    fe.plot(sol.sub(1))
    plt.show()

    print("Vact: ", (fe.assemble(V_act*fe.dx) - V_s/math.pi*math.pi)*10e12)
    print("E: ", fe.assemble(Energy_el))
    #print("En", fe.assemble((r_fce/math.pi - V_act)*fe.dx))
    #print(r_fce)

    if type == "lagrange":
        xx, zz, rr = sol.split(deepcopy=True)
    else:
        xx, zz = sol.split(deepcopy=True)
    fe.plot(xx)
    plt.show()
    plt.plot(xx.vector()[:], zz.vector()[:], "x")
    plt.show()


lipid_transformation_solver(type="nl")

import quadprog
