# -------------------------
# ode_lipid_parametrization_02.py
# -------------------------

# -------------------------
# Description:
# - Based on article of Jon C. Luke
# - ODEs are solved by SciPy - solve_bvp
#
# Last edit: 05.05. 2023
# -------------------------

import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
import scipy.misc
from scipy.integrate import solve_bvp
import math


def area_form_2(y, t):
    "right hand side of ODEs"
    B, T, theta, r, z, A, V, E = y
    dB = 0.5*p*r*np.sin(theta) - T*np.cos(theta)
    dT = 2.0*B/(r**2)*np.cos(theta) - B**2/r - B*c0/r - 0.5*p*np.cos(theta)
    dtheta = np.cos(theta)/r - c0 - B
    dr = np.sin(theta)
    dz = np.cos(theta)
    dA = 2.0*math.pi*r
    dV = math.pi*r**2*np.cos(theta)
    dE = math.pi*r*B**2
    return [dB, dT, dtheta, dr, dz, dA, dV, dE]


def area_form_3(t, y):
    "right hand side of ODEs"
    B, T, theta, r, z, A, V, E = y
    dB = 0.5*p*r*np.sin(theta) - T*np.cos(theta)
    dT = 2.0*B/(r**2)*np.cos(theta) - B**2/r - B*c0/r - 0.5*p*np.cos(theta)
    dtheta = np.cos(theta)/r - c0 - B
    dr = np.sin(theta)
    dz = np.cos(theta)
    dA = 2.0*math.pi*r
    dV = math.pi*r**2*np.cos(theta)
    dE = math.pi*r*B**2
    return [dB, dT, dtheta, dr, dz, dA, dV, dE]


s_start = 0.0
s_end = 5.0
p = 1.0
c0 = 3.0
b_init = -1.0
t_init = 0.5

t= np.linspace(s_start, s_end, 10000)
y0 = [b_init, t_init, 0.5*math.pi, 0.0001, 0.0, 0.0, 0.0, 0.0]

sol1 = scipy.integrate.odeint(area_form_2, y0, t)
sol2 = scipy.integrate.solve_ivp(area_form_3, [s_start, s_end], y0, method="LSODA", t_eval=t)


plt.plot(sol2.t, sol2.y[3, :], label="ivp")
plt.plot(t, sol1[:, 3], label="odeint")
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.show()

plt.plot(t, sol1[:,6])
plt.xlabel('x')
plt.ylabel('y')
plt.show()


plt.plot(sol1[:, 3], sol1[:, 4])
plt.xlabel('x')
plt.ylabel('y')
plt.show()

#gamma = 2*math.pi*R0**2*xi
#alp0 = a0/(2*math.pi*R0**2)