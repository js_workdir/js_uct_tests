# -------------------------
# fem_lipid_1D_flattest_1.py
# -------------------------

# -------------------------
# Description:
# - Test of membrane model with flat start
# - Spontaneous curvature model
#
# Last edit: 10.05. 2023
# -------------------------

import fenics
import fenics as fe
import matplotlib.pyplot as plt
import mpi4py
import ufl
import math
import numpy as np


def lipid_flattest_solver(c0init):
    ##############################
    # Numerical parameters
    ##############################
    num_elements = 100  # Number of elements
    v_deg = 3  # Degree of driving term
    ex_deg = 100  # Degree of expression
    newton_iters = 100  # Number of Newton iters
    s_start = -15  # Start point x
    s_end = 15  # End point x
    s_middle = 0.0

    ##############################
    # Material parameters
    ##############################
    kb = 0.1  # Material parameter
    c0 = c0init

    ##############################
    # Classes
    ##############################
    # Definition of left point
    def left_point(x):
        return fe.near(x[0], s_start)

    # Definition of right point
    def right_point(x):
        return fe.near(x[0], s_end)

    # Definition of middle point
    def middle_point(x):
        return fe.near(x[0], s_middle)

    ##############################
    # Mesh constructor
    ##############################
    mesh = fe.IntervalMesh(num_elements, s_start, s_end)

    ##############################
    # Function spaces
    ##############################
    V = fe.FunctionSpace(mesh, "P", v_deg)
    P1 = fe.FiniteElement("P", fe.interval, v_deg)
    R1 = fe.FiniteElement("Real", fe.interval, 0)
    element = fe.MixedElement([P1, P1])
    Vmix = fe.FunctionSpace(mesh, element)

    ##############################
    # Dirichlet BC
    ##############################
    BC_u_1 = fe.DirichletBC(Vmix.sub(1), 0.0, middle_point, method="pointwise")
    BC_u_2 = fe.DirichletBC(Vmix.sub(1), 0.0, left_point, method="pointwise")
    BC_u_3 = fe.DirichletBC(Vmix.sub(1), 0.0, right_point, method="pointwise")
    BC_u = [BC_u_1, BC_u_2, BC_u_3]

    ##############################
    # Initialization
    ##############################
    c0_init = fe.interpolate(fe.Expression("x[0]<=0.0 ? -c0 : c0", c0=c0, degree=1), V)
    sol = fe.interpolate(fe.Expression(("x[0]", "1.0"), degree=ex_deg), Vmix)
    x_fce, z_fce = fe.split(sol)

    ##############################
    # Energy form
    ##############################
    # Auxiliary variables
    l_d2 = (x_fce.dx(0))**2 + (z_fce.dx(0))**2
    l_d = fe.sqrt(l_d2)
    psi_d = (-x_fce.dx(0).dx(0)*z_fce.dx(0) + x_fce.dx(0)*z_fce.dx(0).dx(0))/l_d2
    H2 = psi_d/l_d - c0_init

    # Total bending energy with area penalization
    Energy_el = 0.5*kb*H2**2*fe.dx + 80*(l_d - fe.Constant((1)))**2*fe.dx

    ##############################
    # Weak form
    ##############################
    t_test = fe.TestFunction(Vmix)
    n = fe.FacetNormal(mesh)
    h = fe.CellDiameter(mesh)
    h_avg = (h('+') + h('-')) / 2.0
    alpha = fe.Constant(800.0)  # Penalty parameter
    d_en = fe.derivative(Energy_el, sol, t_test)
    d_en -= fe.inner(fe.avg(fe.div(fe.grad(sol))), fe.jump(fe.grad(t_test), n))*fe.dS
    d_en -= fe.inner(fe.jump(fe.grad(sol), n), fe.avg(fe.div(fe.grad(t_test))))*fe.dS
    d_en += alpha/h_avg*fe.inner(fe.jump(fe.grad(sol), n), fe.jump(fe.grad(t_test), n))*fe.dS

    ##############################
    # Nonlinear solver
    ##############################
    Jac = fe.derivative(d_en, sol, fe.TrialFunction(Vmix))
    problem = fe.NonlinearVariationalProblem(d_en, sol, BC_u, Jac)
    solver = fe.NonlinearVariationalSolver(problem)
    prm = solver.parameters
    prm['newton_solver']['krylov_solver']['nonzero_initial_guess'] = True
    parameters = {"newton_solver": {"error_on_nonconvergence": False,\
                                    "relative_tolerance": 1e-6,
                                    "absolute_tolerance": 1e-6,
                                    "maximum_iterations": newton_iters}}
    solver.parameters.update(parameters)
    solver.solve()

    ##############################
    # Postprocess
    ##############################
    xx, zz = sol.split(deepcopy=True)
    plt.plot(xx.vector()[:], zz.vector()[:], label=c0init)
    print(fe.assemble(l_d*fe.dx))


c0s = [0.0, 0.01, 0.1, 0.2]
for i in c0s:
    lipid_flattest_solver(i)
plt.legend()
plt.show()
